CHANGELOG

1. Create 'かばん.txt' at macOS 10.12 Sierra (HFS+)

% ls *.txt
かばん.txt
% env LANG=C ls -b *.txt
\343\201\213\343\201\257\343\202\231\343\202\223.txt

UTF-8(NFD): E3818B(\343\201\213) E381AF(\343\201\257) E38299(\343202231) E38293(\343\202\223)
Unicode: \x{304b} \x{306F} \x{3099} \x{3093}

2. git commit & push to bitbucket

3. Check bitbucket directory
	https://bitbucket.org/hkob/test_apfs_nfd/commits/0828ca1efeaf1090e47a660c08b6dd75cd31edde#chg-%E3%81%8B%E3%81%B0%E3%82%93.txt
	The file 'かばん.txt' in the git repository is NFC

